package edu.towson.cosc435.duong.tip_cal_tanduong

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.RadioGroup
import android.widget.TextView
import java.lang.Exception

class TipCalculatorActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val cal_tip_btn = findViewById<Button>(R.id.cal_tip_btn)
        cal_tip_btn?.setOnClickListener { handleClick() }
    }

    private fun handleClick() {
        val total_tv = findViewById<TextView>(R.id.total_tv)
        try {

            val input_tip = findViewById<TextView>(R.id.input_tip)

            val tipInputString: String = input_tip.editableText.toString()

            val radio_gr_assignment2 = findViewById<RadioGroup>(R.id.radio_gr_assignment2)

            val selectedID = radio_gr_assignment2.checkedRadioButtonId

            val calculate_tip: CalculationTipType = when (selectedID) {
                R.id.ten_percent_radio_btn -> CalculationTipType.First
                R.id.twenty_percent_radio_btn -> CalculationTipType.Second
                R.id.thirty_percent_radio_btn -> CalculationTipType.Third
                else -> throw Exception("Please choose a method!!")
            }

            val tipInputDouble: Double = tipInputString.toDouble()
            val result_tip: Double = calculationTip(tipInputDouble, calculate_tip)

            //Display a properly formatted result
            val format = resources.getString(R.string.total_format)
            total_tv.text = String.format(format, result_tip)

        } catch (e: Exception){
            total_tv.text = getString(R.string.error_msg)
        }
    }
}