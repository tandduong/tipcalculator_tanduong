package edu.towson.cosc435.duong.tip_cal_tanduong

enum class CalculationTipType{ First, Second, Third }

var check = 0.0

fun calculationTip(input: Double, calculationTipType: CalculationTipType): Double {
    if (calculationTipType.equals(CalculationTipType.First)) {
        check = input + (input * 10 / 100)
    }

    if (calculationTipType.equals(CalculationTipType.Second)) {
        check = input + (input * 20 / 100)
    }

    if (calculationTipType.equals(CalculationTipType.Third)) {
        check = input + (input * 30 / 100)
    }

    return check
}
